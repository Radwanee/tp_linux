# TP2 : Ethernet, IP, et ARP

## I. Setup IP

**🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines**
Les deux IPs choisies:
```
-192.168.10.2
-192.168.10.1
```
Adresse de réseaux
```
192.168.10.0
```
Adresse de broadcast
```
192.168.10.63
```

Commande utilisée
```
netsh interface ipv4 set address name="Ethernet" static 192.168.10.2 255.255.255.0 192.168.10.1

```
**🌞 Prouvez que la connexion est fonctionnelle entre les deux machines**

Test
```
PS C:\WINDOWS\system32> ping 192.168.10.1

Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.10.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms
```
**🌞 Wireshark it**


**🦈 PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

## II. ARP my bro

**🌞 Check the ARP table**
```
arp -a

Interface : 192.168.10.2 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.10.1          7c-d3-0a-82-6c-ce     dynamique
  ```
**🌞 Manipuler la table ARP**
```
arp -delete
La suppression de l'entrée ARP a échoué : Paramètre incorrect.
```
arp -affichage
```
PS C:\WINDOWS\system32> arp -a

Interface : 192.168.10.2 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.10.1          7c-d3-0a-82-6c-ce     dynamique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  ```
**🌞 Wireshark it**

**🦈 PCAP qui contient les trames ARP**

## II.5 Interlude hackerzz

## III. DHCP you too my brooo

**🌞 Wireshark it**

**🦈 PCAP qui contient l'échange DORA**

## IV. Avant-goût TCP et UDP

**🌞 Wireshark it**

**🦈 PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**
