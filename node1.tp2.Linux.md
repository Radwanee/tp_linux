# TP2 : Manipulation de services

- [Intro](#intro)
- [Partie 1 : Installation et configuration d'un service SSH](#partie-1)
- [Partie 2 : Installation et configuration d'un service FTP](#partie-2)
- [Partie 3 : Création d'un service](#partie-3)
---
# Intro

🌞 **Changer le nom de la machine**

- `sudo hostname <node1>`
radwane@node1

-  `sudo nano /etc/hostname`
  node1.tp2.linux
---
🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  - depuis la VM : `ping 1.1.1.1` fonctionnel
    2 packets transmitted, 2 received, 0% packet loss, time 1002ms
    rtt min/avg/max/mdev = 18.896/21.171/23.447/2.275 ms
  - depuis la VM : `ping ynov.com` fonctionnel
    2 packets transmitted, 2 received, 0% packet loss, time 1002ms
    rtt min/avg/max/mdev = 15.301/16.392/17.484/1.091 ms
  - depuis votre PC : `ping <IP_VM>` fonctionnel
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),

---

# Partie 1

🌞 **Installer le paquet `openssh-server`**

-`sudo apt install openssh-server`
openssh-server is already the newest version (1:8.4p1-6ubuntu2).

---
🌞 **Lancer le service `ssh`**

- `systemctl start ssh`

Authenticating as: Radwane,,, (radwane)
Password:
==== AUTHENTICATION COMPLETE ===

- `systemctl status`

● node1.tp2.linux
    State: running

>  `systemctl enable ssh`

Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ===


---
🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - `systemctl status`

  ● node1.tp2.linux
    State: running

- `ps ssh`

  1000    1775 0000000000000000 0000000000000000 0000000000000000 0000000073d1fef9 R+   pts/1      0:00 ps ssh
---

- `ss -l`

tcp   LISTEN 0      4096                                127.0.0.53%lo:domain                      0.0.0.0:*
tcp   LISTEN 0      128                                       0.0.0.0:ssh                         0.0.0.0:*
tcp   LISTEN 0      128                                     127.0.0.1:ipp                         0.0.0.0:*

- `cd /var/log/`
- `journalctl`

radwane-VirtualBox kernel: NX (Execute Disable) protection: active

---

🌞 **Connectez vous au serveur**

- `ssh radwane@192.168.247.13`
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-20-generic x86_64)

---
🌞 **Modifier le comportement du service**

- dans le fichier `/etc/ssh/sshd_config`
   `sudo nano /etc/ssh/sshd_config`
   `sudo cat /etc/ssh/sshd_config
#Port 1025
#AddressFamily any
#ListenAddress 0.0.0.0`
  -`ss -l`
tcp   LISTEN 0      128                                         [::1]:ipp                            [::]:*
> `systemctl restart`

---
🌞 **Connectez vous sur le nouveau port choisi**

- ssh radwane@192.168.247.13

---

# Partie 2

🌞 **Installer le paquet `vsftpd`**
`apt install vsftpd`
0 upgraded, 1 newly installed, 0 to remove and 27 not upgraded.
---
🌞 **Lancer le service `vsftpd`**

- `systemctl start vsftpd`
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===

- `systemctl status`
 node1.tp2.linux
    State: running

>  `systemctl enable vsftpd`.

---

🌞 **Analyser le service en cours de fonctionnement**

- `ps`
  1550 pts/1    00:00:00 bash
  2933 pts/1    00:00:00 ps

  -`ss -l`
tcp   LISTEN 0      128                                          [::]:ssh                            [::]:*
tcp   LISTEN 0      128                                         [::1]:ipp                            [::]:*

  -  `journalctl`
   radwane-VirtualBox kernel: Hypervisor detected: KVM

  - en consultant un fichier dans `/var/log/`

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
  - les navigateurs Web, ils font ça maintenant
  - demandez moi si vous êtes perdus
- essayez d'uploader et de télécharger un fichier
  - montrez moi à l'aide d'une commande la ligne de log pour l'upload, et la ligne de log pour le download
- vérifier que l'upload fonctionne
  - une fois un fichier upload, vérifiez avec un `ls` sur la machine Linux que le fichier a bien été uploadé

---

🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
- mettez en évidence une ligne de log pour un upload

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

---

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

---

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*
- re-tester l'upload et le download

---

# Partie 3

🌞 **Donnez les deux commandes pour établir ce petit chat avec netcat**

-la commande tapée sur la VM
-la commande tapée sur votre PC

---

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

- utiliser le caractère `>` et/ou `>>` sur la ligne de commande `netcat` de la VM
- cela permettra de stocker les données échangées dans un fichier
- plutôt que de les afficher dans le terminal
- parce quuueeee pourquoi pas ! Ca permet de faire d'autres trucs avec

---

🌞 **Créer un nouveau service**

- créer le fichier `/etc/systemd/system/chat_tp2.service`
- définissez des permissions identiques à celles des aux autres fichiers du même type qui l'entourent
- déposez-y le contenu suivant :

---

🌞 **Tester le nouveau service**

- depuis la VM
  - démarrer le nouveau service avec une commande `systemctl start`
  - vérifier qu'il est correctement lancé avec une commande  `systemctl status`
  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi
- tester depuis votre PC que vous pouvez vous y connecter
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :

---
