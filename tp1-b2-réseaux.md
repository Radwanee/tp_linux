# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

**🌞 Affichez les infos des cartes réseau de votre PC**
--
**nom**
```
* Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
```
**[adresse MAC]**
```
* 20-4E-F6-C0-9A-8D
```
**adresse IP de l'interface WiFi**
```
*  Adresse IPv4. . . . . . . . . . . . . .: 10.33.18.123
```
**nom**
```
* Realtek PCIe GbE Family Controller
```
**[adresse MAC]**
```
* Adresse physique . . . . . . . . . . . : FC-34-97-00-38-84
```
**adresse IP de l'interface Ethernet**
```
* Il n'y a pas d'adresse IP pour l'interface Ethernet
```

**🌞 Affichez votre gateway**
--
```
Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**
--

**IP**
```
* 10.33.18.123
```
**MAC**
```
* 20-4E-F6-C0-9A-8D
```
**Gateway de l'interface WiFi**
```
* 10.33.19.254
```

**🌞 à quoi sert la Gateway dans le réseau d'YNOV ?**

``* La Gateway dans le réseau d'YNOV permet de pouvoir communiquer dans l'ensemble sur le réseau d'YNOV avec les autres pc.
``

## 2. Modifications des informations

**🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :**
```
![Protocole internet](./Protocole%20internet.PNG]
```

🌞 **Il est possible que vous perdiez l'accès internet.**

``* L'accès internet perdu est causé par le changement d'adresse IP que le réseaux ne reconnait pas.``
# II. Exploration locale en duo

## 3. Modification d'adresse IP

**🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :**
```nc
PS C:\Users\Radwa> ping 192.168.69.253

Envoi d’une requête 'Ping'  192.168.69.253 avec 32 octets de données :
Réponse de 192.168.69.253 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.253 : octets=32 temps<1ms TTL=128
Réponse de 192.168.69.253 : octets=32 temps<1ms TTL=128
Réponse de 192.168.69.253 : octets=32 temps<1ms TTL=128
```
```
Statistiques Ping pour 192.168.69.253:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 2ms, Moyenne = 0ms
```
 ```  
C:\Users\Radwa> ipconfig

Carte Ethernet Ethernet :
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.69.254
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
```
```
C:\Users\Radwa> arp -a 192.168.69.253
Interface : 192.168.69.254 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.69.253        7c-d3-0a-82-6c-ce     dynamique
 ```
  
## 4. Utilisation d'un des deux comme Gateway

**🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu**

 **encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait**
```
PS C:\Users\Radwa> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=21 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=28 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=29 ms TTL=54
```

**🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)**

```
C:\Users\adrie>tracert 192.168.137.2

Détermination de l’itinéraire vers LAPTOP-L066RDBA [192.168.137.2]
avec un maximum de 30 sauts :

  1     1 ms     1 ms    <1 ms  LAPTOP-L066RDBA [192.168.137.2]

Itinéraire déterminé.
```

## 5. Petit chat privé

**🌞 **sur le PC _serveur_** avec par exemple l'IP 192.168.1.1**
```
C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888
```

**🌞 **sur le PC _client_** avec par exemple l'IP 192.168.1.2**
```
PS C:\Users\Radwa\Desktop\netcat-1.11> .\nc.exe 192.168.137.1 8888 
cc 
Adrien 
Faut qu'on parle ta mere et moi 
vient dans la chambre fiston
```

**🌞 pour aller un peu plus loin**
```
C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888 192.168.137.2

PS C:\WINDOWS\system32> netstat -a -b -n | select-string 8888

  TCP    192.168.137.1:8888     0.0.0.0:0              LISTENING
  ```

## 6. Firewall

**🌞 Autoriser les `ping`**
```
C:\Users\adrie>ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
```
**🌞 Autoriser le traffic sur le port qu'utilise `nc`**
```
C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888
Salut léo
```
# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

**🌞Exploration du DHCP, depuis votre PC**
```
Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
```
```
Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 16:54:31
```
## 2. DNS

**🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur**
```
Serveurs DNS. . .  . . . . . . . . . . :8.8.8.8
```
**🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main**
Google.com
```
> nslookup google.com
Serveur :   google.com
Addresses:  2a00:1450:4007:808::200e
          216.58.214.174
```
Ynov.com
```
> nslookup ynov.com
Serveur :   ynov.com
Addresses:  2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          2606:4700:20::ac43:4ae2
          172.67.74.226
          104.26.10.233
          104.26.11.233
```
 78.74.21.21  
 ```
Serveur :   dns.google
Address:  8.8.8.8

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```
92.146.54.88
```
Serveur :   dns.google
Address:  8.8.8.8
```
## IV. Wireshark

**🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :**

-   un `ping` entre vous et la passerelle
-   un `netcat` entre vous et votre mate, branché en RJ45
-   une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
-   prenez moi des screens des trames en question
-   on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe
