```bash
video_url="$1"
video_name=$(youtube-dl --get-title $video_url)
youtube-dl -o "/srv/yt/downloads/$video_name/%(title)s.%(ext)s" $video_url
```
