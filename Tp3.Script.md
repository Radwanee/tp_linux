# Tp3 : A lttile script ?

--- 

## I. Script IdCard 

**Entrant** : [Idcard.sh](./idcard.sh)

sortant :
```bash
Machine name: Radwane-VirtualBox
OS : Ubuntu and kernel version is 5.13.0-21-generic
IP : 192.168.247.15/24
RAM : 2025460 1668888
Disque : 2,3 GO left
Top 5 processes by RAM usage :
-  1.0     445 /usr/bin/python3 /usr/bin/networkd-dispatcher --run-startup-triggers
-  4.7     779 /usr/sbin/lightdm-gtk-greeter
-  2.9     556 /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
-  1.1     527 /usr/bin/python3 /usr/share/unattended-upgrades/unattended-upgrade-shutdown --wait-for-signal
-  0.9     768 /usr/bin/pulseaudio --daemonize=no --log-target=journal
Listening ports :

Here's your random cat : https://cdn2.thecatapi.com/images/cid.jpg
```
## II. Script youtube-dl

**Entrant** : [Youtube.sh](./yt.sh)

sortant :
```bash
+ youtube_video_url='https://www.youtube.com/watch?v=L_pw_N8OVDw'
++ youtube-dl --get-title 'https://www.youtube.com/watch?v=L_pw_N8OVDw'
+ video_name='Se Prendre Une Bombe Nucléaire : ÇA FAIT QUOI ?'
+ youtube-dl -o '/srv/yt/downloads/Se Prendre Une Bombe Nucléaire : ÇA FAIT QUOI ?/%(title)s.%(ext)s' 'https://www.youtube.com/watch?v=L_pw_N8OVDw'
```
## III. Make It A Service
